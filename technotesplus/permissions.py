from rest_framework import permissions


class IsOwnAccount(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj == request.user


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user == obj.author


class IsSharedWith(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user.shared_notes.filter(id=obj.id).exists()
