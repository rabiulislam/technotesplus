Tech notes plus

### api
docs http://technotesplus-api.herokuapp.com/api/v1/docs/

admin: https://technotesplus-api.herokuapp.com/admin/

admin username: `admin` password: `TechNotePlus`

### setup and run
- clone this repo and cd into it
- create virtualenv and activate
- run `pip install -r requirements.txt`
- run `python manage.py migrate`
- run `python manage.py runserver`
- go to http://localhost:8000/

### test
run `python manage.py test`

### frontend
Not yet developed due to shortage of time.

