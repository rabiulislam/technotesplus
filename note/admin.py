from django.contrib import admin

from note.models import Note, NoteSharing, Tag


@admin.register(Note)
class NoteAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'author', 'created_at')


@admin.register(NoteSharing)
class NoteSharingAdmin(admin.ModelAdmin):
    pass


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass
