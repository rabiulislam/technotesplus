import logging

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django_q.models import Schedule
from django_q.tasks import schedule, async_task

from note.models import Note, NoteSharing

logger = logging.getLogger(__name__)

User = get_user_model()


msg_template = '''
Hi {0},
{1} have shared a note with you at {2}.
you can find all your shared note in tech note plus dashboard.

Thank you
'''


def send_notification_email(subject, message, email_from, recipient_list):
    logger.info(f'email sending to {recipient_list}')
    send_mail(subject, message, email_from, recipient_list)
    logger.info(f'email sent to {recipient_list}')


def send_notification_to_shared_with_user(note_id, user_id):
    try:
        sharing = NoteSharing.objects.select_related('note', 'user') \
            .get(note_id=note_id, user_id=user_id)

    except NoteSharing.DoesNotExist:
        logger.error(f"note sharing not found for note: {note_id} user: {user_id}")
        return

    if not sharing.seen:
        author_name = sharing.note.author.get_full_name() or sharing.note.author.username
        receiver_name = sharing.user.get_full_name() or sharing.user.username

        subject = f'{author_name} shared a note with you'
        message = msg_template.format(
            receiver_name, author_name, sharing.created_at.strftime("%Y-%m-%d %H:%M:%S"))

        email_from = settings.EMAIL_HOST_USER
        recipient_list = [sharing.user.email]
        async_task('note.utils.send_notification_email',
                   subject, message, email_from, recipient_list,
                   task_name=f'sending email to {recipient_list}')
