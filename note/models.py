from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse

from technotesplus.utils import generate_slug

User = get_user_model()


class Note(models.Model):
    title = models.CharField(
        max_length=127,
        blank=True)

    body = models.TextField()

    author = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        related_name="notes")

    shared_with = models.ManyToManyField(
        to=User,
        blank=True,
        through="NoteSharing",
        related_name="shared_notes")

    tags = models.ManyToManyField(
        to="Tag",
        blank=True,
        related_name="notes")

    created_at = models.DateTimeField(
        auto_now_add=True)

    updated_at = models.DateTimeField(
        auto_now=True)

    class Meta:
        ordering = ('updated_at', '-created_at')

    def get_absolute_url(self):
        return reverse('note-detail', args=[self.id])

    def __str__(self):
        return self.title or self.body[:60]


class NoteSharing(models.Model):
    user = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE)

    note = models.ForeignKey(
        to=Note,
        on_delete=models.CASCADE)

    seen = models.BooleanField(
        default=False)

    notification_sent = models.BooleanField(
        default=False)

    created_at = models.DateTimeField(
        auto_now_add=True)

    class Meta:
        unique_together = ("user", "note")

    def __str__(self):
        return f"Note '{self.note}' shared with '{self.user}'"


class Tag(models.Model):
    name = models.CharField(
        max_length=30)

    slug = models.SlugField(
        max_length=35,
        blank=True,
        null=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_slug(self, self.name)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name
