from django.contrib.auth import get_user_model
from django.urls import reverse
from model_bakery import baker
from rest_framework import status
from rest_framework.test import APITestCase

from note.models import NoteSharing

User = get_user_model()


class TestNoteViewSet(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user(
            username='existing',
            email='existing@user.com',
            password='existing_password')
        baker.make('note.Note', author=cls.user, _quantity=5)
        user_dummy = baker.make(User)
        baker.make('note.Note', author=user_dummy, _quantity=5)

        cls.notes_url = reverse('note-list')
        cls.tags = baker.make('note.Tag', _quantity=5)
        cls.shared_with_user = baker.make('account.User', username='shared_with_user')
        cls.shared_note = baker.make('note.Note', title='shared note 1', author=cls.shared_with_user)
        cls.shared_note.shared_with.add(cls.user)

        cls.note_data = {
            "title": "test note",
            "body": "this is test note",
            "tags": [tag.name for tag in cls.tags]}

    def setUp(self):
        self.client.force_login(self.user)
        self.note = baker.make(
            'note.Note',
            title='test note',
            body='test note body',
            author=self.user,
            tags=self.tags)

    def test_note_create_without_authentication(self):
        self.client.logout()
        response = self.client.post(self.notes_url, self.note_data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_note_create(self):
        response = self.client.post(self.notes_url, self.note_data)
        response_json = response.json()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(response_json.get('id'))
        self.assertEqual(response_json['author'], self.user.id)
        self.assertEqual(response_json['title'], self.note_data['title'])
        self.assertEqual(len(response_json['tags']), len(self.note_data['tags']))

    def test_note_read(self):
        # list
        response = self.client.get(self.notes_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['count'], 6)

        # detail
        response = self.client.get(self.note.get_absolute_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], self.note.id)

    def test_note_update_by_put_request(self):
        updated_data = self.note_data.copy()
        updated_data['title'] = 'updated title'
        updated_data['body'] = 'updated body'
        response = self.client.put(self.note.get_absolute_url(), updated_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['title'], 'updated title')
        self.assertEqual(response.json()['body'], 'updated body')
        self.assertEqual(len(response.json()['tags']), len(self.note_data['tags']))

    def test_note_update_title_by_patch_request(self):
        response = self.client.patch(self.note.get_absolute_url(), {'title': 'updated title'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['title'], 'updated title')
        self.assertEqual(len(response.json()['tags']), len(self.note_data['tags']))

    def test_note_update_body_by_patch_request(self):
        response = self.client.put(self.note.get_absolute_url(), {'body': 'updated body'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['body'], 'updated body')
        self.assertEqual(len(response.json()['tags']), len(self.note_data['tags']))

    def test_note_update_tags_by_put_request(self):
        # before update
        response = self.client.get(self.note.get_absolute_url())
        self.assertEqual(len(response.json()['tags']), len(self.tags))

        # update
        updated_data = self.note_data.copy()
        updated_data['tags'] = ['updated tag 1', 'updated tag 2']
        response = self.client.put(self.note.get_absolute_url(), updated_data)
        response_tags = response.json()['tags']
        self.assertEqual([i['name'] for i in response_tags], ['updated tag 1', 'updated tag 2'])
        self.assertEqual(len(response_tags), 2)

    def test_note_delete(self):
        response = self.client.delete(self.note.get_absolute_url())
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_unauthorized_note_read(self):
        user2 = baker.make('account.User', username='user2')
        note2 = baker.make('note.Note', body='note2', author=user2)
        response = self.client.get(note2.get_absolute_url())
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_notes_search(self):
        response = self.client.get(self.notes_url, data={"search": 'test'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['count'], 1)

    def test_filter_notes_based_on_tags(self):
        response = self.client.get(self.notes_url, data={"tags": self.tags[0].name})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['count'], 1)

    def test_shared_note_list(self):
        response = self.client.get(reverse('shared-note-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['count'], 1)
        self.assertEqual(response.json()['results'][0]['title'], 'shared note 1')

    def test_delete_shared_note(self):
        response = self.client.delete(reverse('shared-note-detail', args=[self.shared_note.id]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    # todo add patch for notification scheduler
    def test_note_sharing(self):
        shared_with_user = baker.make('account.User')
        response = self.client.post(self.note.get_absolute_url() + 'share/',
                                    data={'user': shared_with_user.id})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['status'], True)
        self.assertEqual(response.json()['message'], 'Shared successfully')
        self.assertTrue(self.note.shared_with.filter(id=shared_with_user.id).exists())

    def test_note_sharing_with_self(self):
        url = reverse('note-share', args=[self.note.id])
        response = self.client.post(url, data={'user': self.user.id})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json()['user'][0], "Can't be shared with note author")

    def test_note_sharing_with_already_shared_user(self):
        shared_with_user = baker.make('account.User')
        self.note.shared_with.add(shared_with_user)
        url = reverse('note-share', args=[self.note.id])
        response = self.client.post(url, data={'user': shared_with_user.id})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json()['user'][0], "Already shared with this user")

    def test_unauthorized_note_sharing(self):
        other_user = baker.make('account.User')
        other_users_note = baker.make('note.Note', author=other_user)
        url = reverse('note-share', args=[other_users_note.id])
        response = self.client.post(url, data={'user': self.user.id})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_unsharing_a_shared_note(self):
        shared_with_user = baker.make('account.User')
        self.note.shared_with.add(shared_with_user)
        url = reverse('note-unshare', args=[self.note.id])
        response = self.client.post(url, data={'user': shared_with_user.id})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['status'], True)
        self.assertEqual(response.json()['message'], 'Unshared successfully')
        self.assertFalse(self.note.shared_with.filter(id=shared_with_user.id).exists())

    def test_unsharing_a_not_shared_note(self):
        not_shared_user = baker.make('account.User')
        url = reverse('note-unshare', args=[self.note.id])
        response = self.client.post(url, data={'user': not_shared_user.id})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json()['user'][0], "Note did'nt shared with this user")

    def test_shared_note_seen(self):
        new_note = baker.make('note.Note')
        new_note.shared_with.add(self.user)
        sharing = NoteSharing.objects.get(note=new_note, user=self.user)
        self.assertEqual(sharing.seen, False)

        response = self.client.post(reverse('shared-note-seen', args=[new_note.id]))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        sharing.refresh_from_db()
        self.assertEqual(sharing.seen, True)
