from django.urls import path, include
from rest_framework import routers

from note.views import NoteViewSet, TagViewSet, SharedNoteListView

router = routers.DefaultRouter()
router.register(r'notes', NoteViewSet, basename='note')
router.register(r'shared-notes', SharedNoteListView, basename='shared-note')
router.register(r'tags', TagViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
