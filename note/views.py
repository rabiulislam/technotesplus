import logging

from django.contrib.auth import get_user_model
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from note.models import Note, Tag, NoteSharing
from note.serializers import NoteSerializer, TagSerializer, SharedNoteSerializer, \
    NoteShareSerializer, NoteUnshareSerializer
from note.filters import NoteFilterSet
from technotesplus.permissions import IsSharedWith, IsOwner

logger = logging.getLogger(__name__)
User = get_user_model()


class SharedNoteListView(viewsets.ReadOnlyModelViewSet):
    serializer_class = SharedNoteSerializer
    permission_classes = (IsAuthenticated, IsSharedWith)
    filter_backends = (DjangoFilterBackend, SearchFilter)
    filter_class = NoteFilterSet
    search_fields = ('title', 'body')

    def get_queryset(self):
        return Note.objects.prefetch_related('tags') \
            .filter(shared_with=self.request.user).all()

    @action(detail=True, methods=['post'], permission_classes=(IsAuthenticated, IsSharedWith))
    def seen(self, request, pk=None):
        note = self.get_object()
        try:
            sharing = NoteSharing.objects.get(note=note, user=request.user)
            sharing.seen = True
            sharing.save()

        except NoteSharing.DoesNotExist:
            logger.error(f'sharing object not found for note: {pk} user: {request.user}')

        except Exception as e:
            logger.error(e)

        return Response({'status': True, 'message': 'Seen successfully'})


class TagViewSet(viewsets.ModelViewSet):
    serializer_class = TagSerializer
    queryset = Tag.objects.all()
    filter_backends = [SearchFilter]
    search_fields = ('name', )


class NoteViewSet(viewsets.ModelViewSet):
    serializer_class = NoteSerializer
    permission_classes = (IsAuthenticated, IsOwner)
    filter_backends = (DjangoFilterBackend, SearchFilter)
    filter_class = NoteFilterSet
    search_fields = ('title', 'body')

    def get_queryset(self):
        return Note.objects.prefetch_related('tags', 'shared_with') \
            .filter(author=self.request.user).all()

    @action(detail=True, methods=['post'], permission_classes=(IsAuthenticated, IsOwner))
    def share(self, request, pk=None):
        self.get_object()  # called for object level permission check!
        data = request.data.copy()
        data['note'] = pk
        serializer = NoteShareSerializer(data=data)
        if serializer.is_valid():
            serializer.share()
            return Response({'status': True, 'message': 'Shared successfully'})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'], permission_classes=(IsAuthenticated, IsOwner))
    def unshare(self, request, pk=None):
        self.get_object()  # called for object level permission check!
        data = request.data.copy()
        data['note'] = pk
        serializer = NoteUnshareSerializer(data=data)
        if serializer.is_valid():
            serializer.unshare()
            return Response({'status': True, 'message': 'Unshared successfully'})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
