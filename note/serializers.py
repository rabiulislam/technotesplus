from datetime import timedelta

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils import timezone
from django_q.models import Schedule
from django_q.tasks import schedule
from rest_framework import serializers

from account.serializers import UserMiniSerializer
from note.models import Note, Tag, NoteSharing
from technotesplus.settings import env

User = get_user_model()


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


class NoteSharingSerializer(serializers.ModelSerializer):
    user = UserMiniSerializer(read_only=True)

    class Meta:
        model = NoteSharing
        fields = ('user', 'seen', 'created_at')


class NoteSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    shared_with = NoteSharingSerializer(
        source='notesharing_set', many=True, read_only=True)

    class Meta:
        model = Note
        fields = ('id', 'title', 'body', 'author', 'shared_with',
                  'tags', 'created_at', 'updated_at')
        read_only_fields = ('id', 'author')

    def validate(self, attrs):
        request = self.context['request']
        try:
            attrs['tags'] = request.data.getlist('tags', [])
        except AttributeError:
            pass
        attrs['author'] = request.user
        return attrs

    def create(self, validated_data):
        tag_names = validated_data.pop('tags', None)
        instance = Note.objects.create(**validated_data)
        if tag_names:
            for tag_name in tag_names:
                tag, _ = Tag.objects.get_or_create(name=tag_name)
                instance.tags.add(tag)
        return instance

    def update(self, instance, validated_data):
        tag_names = validated_data.pop('tags', None)
        instance = super().update(instance, validated_data)
        if tag_names:
            instance.tags.clear()
            for tag_name in tag_names:
                tag, _ = Tag.objects.get_or_create(name=tag_name)
                instance.tags.add(tag)
        return instance


class SharedNoteSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = Note
        exclude = ('shared_with', 'created_at', 'updated_at')


class BaseNoteShareUnshareSerializer(serializers.Serializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    note = serializers.PrimaryKeyRelatedField(queryset=Note.objects.all())


class NoteShareSerializer(BaseNoteShareUnshareSerializer):
    def validate(self, attrs):
        note = attrs.get('note')
        user = attrs.get('user')
        if user == note.author:
            raise ValidationError({"user": "Can't be shared with note author"})
        if note.shared_with.filter(id=user.id).exists():
            raise ValidationError({"user": "Already shared with this user"})
        return attrs

    def share(self):
        user = self.validated_data['user']
        note = self.validated_data['note']
        note.shared_with.add(user)

        # send notification email after certain intervals
        interval = env.int('NOTIFICATION_EMAIL_INTERVAL_MINUTES', 5)
        schedule('note.utils.send_notification_to_shared_with_user',
                 note.id, user.id,
                 schedule_type=Schedule.ONCE,
                 next_run=timezone.now() + timedelta(minutes=interval))


class NoteUnshareSerializer(BaseNoteShareUnshareSerializer):
    def validate(self, attrs):
        note = attrs.get('note')
        user = attrs.get('user')
        if not note.shared_with.filter(id=user.id).exists():
            raise ValidationError({"user": "Note did'nt shared with this user"})
        return attrs

    def unshare(self):
        user = self.validated_data['user']
        note = self.validated_data['note']
        note.shared_with.remove(user)
