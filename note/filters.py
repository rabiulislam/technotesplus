import django_filters

from note.models import Note


class NoteFilterSet(django_filters.filterset.FilterSet):
    tags = django_filters.CharFilter(lookup_expr='slug__iexact')

    class Meta:
        model = Note
        fields = ('tags',)
